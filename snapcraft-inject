#! /usr/bin/env python3

import glob
import os
import subprocess


SNAPCRAFT_INJECT_ROOT = os.environ["SNAPCRAFT_INJECT_ROOT"]


class Lxc:
    def __init__(self, container_name):
        self.container_name = container_name

    def command(self, params):
        return subprocess.check_output(
            f"lxc --project snapcraft {params}", shell=True, text=True
        ).splitlines()

    def exec(self, params):
        return self.command(f"exec {self.container_name} -- {params}")

    def start(self):
        self.command(f"start {self.container_name}")

    def stop(self):
        self.command(f"stop {self.container_name}")

    def list_parts(self):
        return self.exec("ls parts")

    def list_stage_snaps(self, part):
        if "stage_snaps" in self.exec(f"ls parts/{part}"):
            return [
                f
                for f in self.exec(f"ls parts/{part}/stage_snaps")
                if f.endswith(".snap")
            ]
        else:
            return []

    def replace_stage_snap(self, part, stage_snap, local_snap):
        print(f"Replacing {part}'s stage snap {stage_snap} with {local_snap}")
        base_name = os.path.basename(local_snap)
        stage_assert = os.path.splitext(stage_snap)[0] + ".assert"

        self.command(f"file push {local_snap} {self.container_name}/tmp/{base_name}")
        self.exec(f"rm parts/{part}/stage_snaps/{stage_snap}")
        self.exec(f"rm parts/{part}/stage_snaps/{stage_assert}")
        self.exec(f"mv /tmp/{base_name} parts/{part}/stage_snaps")

    def list_installed_snaps(self):
        return self.exec("snap list | grep -v Version | awk '{print $1;}'")

    def replace_installed_snap(self, snap_name, local_snap):
        print(f"Replacing installed snap: {snap_name} with {local_snap}")
        self.command(
            f"file push {local_snap} {self.container_name}/tmp/{snap_name}.snap"
        )
        self.exec(f"snap remove {snap_name}")
        self.exec(f"snap install --devmode /tmp/{snap_name}.snap")
        self.exec(f"rm /tmp/{snap_name}.snap")


def pull_and_get_container_name():
    print("Please wait, executing snapcraft pull and trying to find container name")
    command = """
        snapcraft pull --verbosity=trace 2>&1 | 
        grep 'Executing on host: lxc --project snapcraft stop' | 
        awk '{print $NF;}'
    """
    container_name = subprocess.check_output(command, shell=True, text=True).strip()
    return container_name


def find_local_snaps():
    return glob.glob(SNAPCRAFT_INJECT_ROOT + "/**/*.snap", recursive=True)


def snap_base_name(snap_name):
    base_name = os.path.basename(snap_name)
    base_name = os.path.splitext(base_name)[0]
    return base_name.split("_")[0]


def main():
    local_snaps = find_local_snaps()
    local_snaps = {snap_base_name(snap): snap for snap in local_snaps}

    lxc = Lxc(pull_and_get_container_name())

    lxc.start()

    installed_snaps = lxc.list_installed_snaps()

    parts = lxc.list_parts()
    stage_snaps_by_part = {part: lxc.list_stage_snaps(part) for part in parts}
    stage_snaps_by_part = {
        part: snaps for part, snaps in stage_snaps_by_part.items() if snaps
    }

    # Replace installed snaps
    for snap_name, local_snap in local_snaps.items():
        if snap_name in installed_snaps:
            lxc.replace_installed_snap(snap_name, local_snap)

    # Replace stage snaps in each part
    for part, stage_snaps in stage_snaps_by_part.items():
        for stage_snap in stage_snaps:
            stage_snap_name = snap_base_name(stage_snap)
            if stage_snap_name in local_snaps:
                local_snap = local_snaps[stage_snap_name]
                lxc.replace_stage_snap(part, stage_snap, local_snap)

    lxc.stop()


if __name__ == "__main__":
    main()
