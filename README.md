# Snap Helpers

This repository contains scripts to help building snaps using snapcraft. In particular it tries to address the need for developers to build things locally testing dependencies without having to publish on the store at each step.

## snapcraft-inject

### Setup

For easier use, `snapcraft-inject` needs to be in your path. It assumes the `SNAPCRAFT_INJECT_ROOT` environment variable is set and points to a directory containing the snaps to inject.

`snapcraft-inject` will look into `SNAPCRAFT_INJECT_ROOT` for snaps to inject, but it will do so recursively. This is especially convenient if you have several repositories coming from [snap-packaging](https://invent.kde.org/neon/snap-packaging) in a single directory. You just need to point the environment to said directory.

For instance, let's assume a `$HOME/snap-packaging` directory containing clones of `kf6-snap` and `kf6-snap-runtime`. Then setting `SNAPCRAFT_INJECT_ROOT` to `$HOME/snap-packaging` is probably a good choice.

Also this script will assume you're using the lxd build provider with snapcraft. So if not already done, make sure you installed lxd properly and run `snap set snapcraft provider=lxd`. For more details about build providers and how to set them up, check out the [snapcraft documentation about build providers](https://snapcraft.io/docs/build-providers).

### Building snaps using locally built dependencies

`snapcraft-inject` should be seen as an alternative to the `snapcraft pull` lifecycle command of `snapcraft`. In fact, under the hood `snapcraft-inject` will run `snapcraft pull` to do its job. For more information, check out the documentation about [snapcraft parts lifecycle](https://snapcraft.io/docs/parts-lifecycle).

Once the setup is done, building a snap using your locally built snaps as dependencies should be as simple as running:

* `snapcraft-inject && snapcraft`

### How it works

`snapcraft-inject` does the following:

* runs a `snapcraft pull` in trace mode to find the lxd container used to build the snap
* then it gets into the container to look for stage-snaps in each parts and for installed build-snaps
* for each snap found, it looks for a replacement in `SNAPCRAFT_INJECT_ROOT`
  * when a replacement matches, the original is removed and the replacement is put in its place

When `snapcraft` is run after `snapcraft-inject` the following will happen:

* the `pull` step will be skipped since `snapcraft` knows it already happened
* all the dependencies replaced by `snapcraft-inject` will thus stay in place
* `snapcraft` will carry on with the other steps all the way to packaging

### Dependencies

This is the usual you would need to use snapcraft productively so:

* snap
* snapcraft
* lxd

This is also script so to run it needs:

* python3

